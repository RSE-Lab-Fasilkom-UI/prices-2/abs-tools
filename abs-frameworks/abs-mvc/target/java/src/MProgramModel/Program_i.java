package MProgramModel;
// Program.abs:6:0: 
public interface Program_i extends abs.backend.java.lib.types.ABSInterface {
    // Program.abs:8:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setIdProgram(abs.backend.java.lib.types.ABSInteger idProgram);
    // Program.abs:8:1: 
    public  abs.backend.java.lib.types.ABSUnit setIdProgram(abs.backend.java.lib.types.ABSInteger idProgram);
    // Program.abs:9:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSInteger> async_getIdProgram();
    // Program.abs:9:1: 
    public  abs.backend.java.lib.types.ABSInteger getIdProgram();
    // Program.abs:10:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setNamaProgram(abs.backend.java.lib.types.ABSString namaProgram);
    // Program.abs:10:1: 
    public  abs.backend.java.lib.types.ABSUnit setNamaProgram(abs.backend.java.lib.types.ABSString namaProgram);
    // Program.abs:11:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getNamaProgram();
    // Program.abs:11:1: 
    public  abs.backend.java.lib.types.ABSString getNamaProgram();
    // Program.abs:12:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setDepartemen(abs.backend.java.lib.types.ABSString departemen);
    // Program.abs:12:1: 
    public  abs.backend.java.lib.types.ABSUnit setDepartemen(abs.backend.java.lib.types.ABSString departemen);
    // Program.abs:13:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getDepartemen();
    // Program.abs:13:1: 
    public  abs.backend.java.lib.types.ABSString getDepartemen();
    // Program.abs:14:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setTempat(abs.backend.java.lib.types.ABSString tempat);
    // Program.abs:14:1: 
    public  abs.backend.java.lib.types.ABSUnit setTempat(abs.backend.java.lib.types.ABSString tempat);
    // Program.abs:15:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getTempat();
    // Program.abs:15:1: 
    public  abs.backend.java.lib.types.ABSString getTempat();
    // Program.abs:16:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setTanggal(abs.backend.java.lib.types.ABSString tanggal);
    // Program.abs:16:1: 
    public  abs.backend.java.lib.types.ABSUnit setTanggal(abs.backend.java.lib.types.ABSString tanggal);
    // Program.abs:17:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getTanggal();
    // Program.abs:17:1: 
    public  abs.backend.java.lib.types.ABSString getTanggal();
    // Program.abs:18:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setPeserta(abs.backend.java.lib.types.ABSInteger peserta);
    // Program.abs:18:1: 
    public  abs.backend.java.lib.types.ABSUnit setPeserta(abs.backend.java.lib.types.ABSInteger peserta);
    // Program.abs:19:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSInteger> async_getPeserta();
    // Program.abs:19:1: 
    public  abs.backend.java.lib.types.ABSInteger getPeserta();
    // Program.abs:20:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setPenanggungJawab(abs.backend.java.lib.types.ABSString penanggungJawab);
    // Program.abs:20:1: 
    public  abs.backend.java.lib.types.ABSUnit setPenanggungJawab(abs.backend.java.lib.types.ABSString penanggungJawab);
    // Program.abs:21:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getPenanggungJawab();
    // Program.abs:21:1: 
    public  abs.backend.java.lib.types.ABSString getPenanggungJawab();
    // Program.abs:22:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSUnit> async_setBiaya(abs.backend.java.lib.types.ABSRational biaya);
    // Program.abs:22:1: 
    public  abs.backend.java.lib.types.ABSUnit setBiaya(abs.backend.java.lib.types.ABSRational biaya);
    // Program.abs:23:1: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSRational> async_getBiaya();
    // Program.abs:23:1: 
    public  abs.backend.java.lib.types.ABSRational getBiaya();
}
