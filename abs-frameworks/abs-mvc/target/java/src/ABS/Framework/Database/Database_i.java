package ABS.Framework.Database;
// Database.abs:7:0: 
public interface Database_i extends abs.backend.java.lib.types.ABSInterface {
    // Database.abs:7:21: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_connect(abs.backend.java.lib.types.ABSString msg);
    // Database.abs:7:21: 
    public  abs.backend.java.lib.types.ABSString connect(abs.backend.java.lib.types.ABSString msg);
}
