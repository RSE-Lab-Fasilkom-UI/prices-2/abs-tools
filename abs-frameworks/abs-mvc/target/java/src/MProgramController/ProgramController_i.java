package MProgramController;
// ProgramController.abs:7:0: 
public interface ProgramController_i extends abs.backend.java.lib.types.ABSInterface {
    // ProgramController.abs:9:1: 
    public  abs.backend.java.lib.runtime.ABSFut<ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>>> async_list(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:9:1: 
    public  ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>> list(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:10:1: 
    public  abs.backend.java.lib.runtime.ABSFut<ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>>> async_add(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:10:1: 
    public  ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>> add(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:11:1: 
    public  abs.backend.java.lib.runtime.ABSFut<ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>>> async_save(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:11:1: 
    public  ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>> save(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:12:1: 
    public  abs.backend.java.lib.runtime.ABSFut<ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>>> async_edit(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:12:1: 
    public  ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>> edit(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:13:1: 
    public  abs.backend.java.lib.runtime.ABSFut<ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>>> async_update(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:13:1: 
    public  ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>> update(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:14:1: 
    public  abs.backend.java.lib.runtime.ABSFut<ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>>> async_delete(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:14:1: 
    public  ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>> delete(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:15:1: 
    public  abs.backend.java.lib.runtime.ABSFut<ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>>> async_detail(ABS.Framework.Http.ABSHttpRequest_i request);
    // ProgramController.abs:15:1: 
    public  ABS.StdLib.Pair<abs.backend.java.lib.types.ABSString,ABS.StdLib.List<MProgramModel.Program_i>> detail(ABS.Framework.Http.ABSHttpRequest_i request);
}
